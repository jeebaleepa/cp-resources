/*
 __  __ _       _                     _    _ 
|  \/  (_)     | |                   | |  (_)
| \  / |_ _ __ | | _______      _____| | ___ 
| |\/| | | '_ \| |/ / _ \ \ /\ / / __| |/ / |
| |  | | | | | |   < (_) \ V  V /\__ \   <| |
|_|  |_|_|_| |_|_|\_\___/ \_/\_/ |___/_|\_\_| 

*/

vector<pointDouble> minkowski(vector<pointDouble>v,vector<pointDouble>q)
{
    int n=v.size();
    int m=q.size();
    ///sort v;
    for(int i=0;i<n;i++)
    {
        if(v[i].y<v[0].y ||(v[i].y==v[0].y && v[i].x<v[0].x))
            swap(v[0],v[i]);
    }
 
    pointDouble vp0;
    vp0=v[0];
    sort(v.begin()+1, v.end(), [&](const pointDouble& a, const pointDouble& b) {
        return vp0.angleRadian(b,a)>0 ||
			   (abs(vp0.angleRadian(b,a))<eps && vp0.dist(a)<vp0.dist(b));
    });
 
    for(int i=0;i<m;i++)
    {
        if(q[i].y<q[0].y ||(q[i].y==q[0].y && q[i].x<q[0].x))
            swap(q[0],q[i]);
    }
 
    pointDouble qp0;
    qp0=q[0];
    sort(q.begin()+1, q.end(), [&](const pointDouble& a, const pointDouble& b) {
        return qp0.angleRadian(b,a)>0 ||
			   (abs(qp0.angleRadian(b,a))<eps && qp0.dist(a)<qp0.dist(b));
    });
 
    pointDouble start=qp0+vp0;
 
    vector<pointDouble>ans;
    ans.pb(start);
    int cntv=0;
    int cntq=0;
 
    pointDouble vx;
    pointDouble qx;
    pointDouble nun;
    nun.x=0;nun.y=0;
    while(cntv<n && cntq<m)
    {
        vx=v[(cntv+1)%n];
        qx=q[(cntq+1)%m];
 
        if(nun.sideofpoint(vx-v[cntv],qx-q[cntq])!=-1)
        {
            start+=v[(cntv+1)%n]-v[cntv];
            ans.pb(start);
            cntv++;
        }
        else
        {
            start+=q[(cntq+1)%m]-q[cntq];
            ans.pb(start);
            cntq++;
        }
    }
    while(cntv<n)
    {
        start+=v[(cntv+1)%n]-v[cntv];
        ans.pb(start);
        cntv++;
    }
    while(cntq<m)
    {
        start+=q[(cntq+1)%m]-q[cntq];
        ans.pb(start);
        cntq++;
    }
    ans.pop_back();
    return ans;
 
}
