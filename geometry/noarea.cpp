/*
 _   _  ____                         
| \ | |/ __ \   /\                   
|  \| | |  | | /  \   _ __ ___  __ _ 
| . ` | |  | |/ /\ \ | '__/ _ \/ _` |
| |\  | |__| / ____ \| | |  __/ (_| |
|_| \_|\____/_/    \_\_|  \___|\__,_|

*/

vector<circle> c;
int p[200200];
void init(int n)
{
    for(int i=0;i<=n;i++)
        p[i]=i;
}
int par(int i)
{
    if(p[i]==i)
        return i;
    return p[i]=par(p[i]);
}
void merg(int i,int j)
{
    i=par(i);
    j=par(j);
    p[i]=j;
    return ;
}
int number_of_area()
{
    int n;
    cin>>n;
    circle cir;
    for(int i=0;i<n;i++)
    {
        cir.read();
        int f=1;
        for(auto ci:c)
        {
            if(ci==cir)
                f=0;
        }
        if(f)
            c.pb(cir);
    }
    n=c.size();
    init(n);
    /// f=e-v+c+1;
    int e=0,v=0,co=0;

    set<point>all;
    for(int i=0;i<n;i++)
    {
        set<point>tmp;
        for(int j=0;j<n;j++)
        {
            if(i==j)
                continue;
            point p1,p2;
            int f=intersectcirclecircl(c[i],c[j],p1,p2);
            if(f==0)
                continue;
            else if(f==1)
            {
                tmp.insert(p1);
                all.insert(p1);
                merg(i,j);
            }
            else
            {
                all.insert(p1);
                all.insert(p2);
                tmp.insert(p1);
                tmp.insert(p2);
                merg(i,j);
            }
        }
        e+=tmp.size();
    }
    v=all.size();
    for(int i=0;i<n;i++)
        co+=(par(i)==i);
    return e-v+co+1;

}
