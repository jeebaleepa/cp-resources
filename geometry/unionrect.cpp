/*
 _    _       _             _____           _   
| |  | |     (_)           |  __ \         | |  
| |  | |_ __  _  ___  _ __ | |__) |___  ___| |_ 
| |  | | '_ \| |/ _ \| '_ \|  _  // _ \/ __| __|
| |__| | | | | | (_) | | | | | \ \  __/ (__| |_ 
 \____/|_| |_|_|\___/|_| |_|_|  \_\___|\___|\__|
                                                
*/

#include <bits/stdc++.h>
#define all(x) x.begin(),x.end()
#define sc(x) scanf("%d",&x)
#define scl(x) scanf("%lld",&x)
#define pr(x) printf("%d\n",x)
#define prl(x) printf("%lld\n",x)
#define yes printf("YES\n")
#define no printf ("NO\n")
#define ll long long
#define ld long double
#define pb push_back
#define f first
#define s second
#define tmp (l+r)/2
#define fast ios::sync_with_stdio(0);cin.tie(0);cout.tie(0);
const ld pi=2*acos(0);
const ll mod=1e9+7;
const ld eps=1e-9;
const ll ll_max=2e18;
///const ll mod=998244353;
const int N=1e9;
using namespace std;
struct node
{
    ll sum;
    ll prop;
    ll ans;
    node()
    {
        sum=0;
        prop=0;
        ans=0;
    }
};
struct HASH{
  size_t operator()(const pair<int,int>&x)const{
    return hash<long long>()(((long long)x.first)^(((long long)x.second)<<32));
  }
};
unordered_map<pair<ll,ll>,node,HASH>seg;
ll ok(ll l,ll r)
{
    if(seg[{l,r}].prop>0)
        return r-l+1;
    else
        return seg[{l,r}].ans;
}
void update(ll x,ll y,ll v,ll l=-N,ll r=N)
{
    if(x>r || y<l)
        return ;
    if(x<=l && y>=r)
    {
        seg[{l,r}].prop+=v;
        return ;
    }
    update(x,y,v,l,tmp);
    update(x,y,v,tmp+1,r);
    seg[{l,r}].ans=ok(l,tmp)+ok(tmp+1,r);
    return ;
}
struct event{
    ll x;
    ll type;
    ll l;
    ll r;
    event(){
        x=0;type=0;l=0;r=0;
    }
    bool operator < (event e)
    {
        return make_pair((*this).x,(*this).type)<make_pair(e.x,e.type);
    }
};
vector<event>vec;
int main()
{
    int n;
    sc(n);
    for(int i=0;i<n;i++)
    {
        ll a,b,c,d;
        scl(a);scl(b);
        scl(c);scl(d);
        a+=2;b+=2;c+=2;d+=2;
        event e;
        e.x=a;
        e.type=1;
        e.l=b;
        e.r=d-1;
        vec.pb(e);
        e.x=c;
        e.type=-1;
        e.l=b;
        e.r=d-1;
        vec.pb(e);
    }
    sort(all(vec));
    int w=2*n;
    ll last=vec[0].x;
    update(vec[0].l,vec[0].r,1);
    ll ans=0;
    for(int i=1;i<w;i++)
    {
        ans+=(vec[i].x-last)*ok(-N,N);
        update(vec[i].l,vec[i].r,vec[i].type);
        last=vec[i].x;
    }
    prl(ans);
}
