/*
  _____            _____              _     _      
 / ____|          |  __ \            | |   | |     
| |  __  ___  ___ | |  | | ___  _   _| |__ | | ___ 
| | |_ |/ _ \/ _ \| |  | |/ _ \| | | | '_ \| |/ _ \
| |__| |  __/ (_) | |__| | (_) | |_| | |_) | |  __/
 \_____|\___|\___/|_____/ \___/ \__,_|_.__/|_|\___|
                                                   
*/

struct pointDouble
{
    long double x,y;

    void read()
    {
        cin>>x>>y;
    }
    void write()
    {
        cout<<x<<" "<<y<<endl;
    }
    pointDouble operator + ( pointDouble p )
    {
        return pointDouble { x+p.x, y+p.y };
    }
    void operator += ( pointDouble p )
    {
        x += p.x;
        y += p.y;
    }
    pointDouble operator - ( pointDouble p )
    {
        return pointDouble { x-p.x, y-p.y };
    }
    void operator -= ( pointDouble p )
    {
        x -= p.x;
        y -= p.y;
    }
    bool operator == (const pointDouble& b)
    {
        return abs(x-b.x) <eps && abs(y-b.y)<eps;
    }
    bool operator != (const pointDouble& b)
    {
        return !( (*this) == b );
    }


    long double operator * ( pointDouble p ) const
    {
        return x*p.y - p.x*y;
    }
    bool operator<(const pointDouble& p) const
    {
        return x < p.x - eps || (abs(x - p.x) < eps && y < p.y - eps);
    }

    /// 0 same, -1 c is in the right, else 1
    int sideofpoint ( pointDouble b, pointDouble c )
    {
        long double res = (b-*this)*(c-*this);

        if( res == 0 )
            return 0;
        if( res > 0 )
            return 1;
        return -1;
    }

    pointDouble rotateAboutRadian( pointDouble center, long double theta )
    {
        pointDouble ret = (*this);

        ret -= center;

        long double x = ret.x;
        long double y = ret.y;

        long double r = sqrt(x*x+y*y);

        long double t = acos(x/r);

        if( y < 0 )
            t = -t;

        t += theta;

        x = r * cos(t);
        y = r * sin(t);

        x += center.x;
        y += center.y;

        ret.x = x;
        ret.y = y;

        return ret;
    }

    long double angleDegree(pointDouble p1,pointDouble p2)
    {
        /// angle from p2 to p1
        p1 -= (*this);
        p2 -= (*this);

        long double r1 = sqrt(p1.x*p1.x+p1.y*p1.y);
        long double t1 = acos(p1.x/r1);
        if( p1.y < 0 )
            t1 = -t1;

        long double r2 = sqrt(p2.x*p2.x+p2.y*p2.y);
        long double t2 = acos(p2.x/r2);
        if( p2.y < 0 )
            t2 = -t2;

        long double ret = t1-t2;

        ret = 180.0*ret/pi;

        return ret;
    }

    long double angleRadian(pointDouble p1,pointDouble p2)
    {
        /// angle from p2 to p1
        p1 -= (*this);
        p2 -= (*this);

        long double r1 = sqrt(p1.x*p1.x+p1.y*p1.y);
        long double t1 = acos(p1.x/r1);
        if( p1.y < 0 )
            t1 = -t1;

        long double r2 = sqrt(p2.x*p2.x+p2.y*p2.y);
        long double t2 = acos(p2.x/r2);
        if( p2.y < 0 )
            t2 = -t2;

        long double ret = t1-t2;

        return ret;
    }

    long double dist(pointDouble p1)
    {
        pointDouble p2 = (*this);
        return sqrt( (p1.x-p2.x)*(p1.x-p2.x) + (p1.y-p2.y)*(p1.y-p2.y) );
    }

    pointDouble reflectFromLine(pointDouble p1,pointDouble p2)
    {
        long double t = p1.angleRadian(p2,*(this));

        pointDouble newp2 = p2.rotateAboutRadian(p1,t);

        long double rtot = newp2.dist(p1);

        long double r = (*this).dist(p1);

        pointDouble ret;

        newp2 -= p1;

        ret.x = newp2.x*r/rtot;
        ret.y = newp2.y*r/rtot;

        ret += p1;

        return ret;
    }

    long double distFromLine(pointDouble p1,pointDouble p2)
    {
        pointDouble reflect = (*this).reflectFromLine(p1,p2);

        return reflect.dist((*this))/2.0;
    }

    pointDouble moveTowards(pointDouble p1,long double d)
    {
        pointDouble p = (*this);

        long double rtot = p.dist(p1);

        long double r = d;

        p1 -= p;

        pointDouble ret;

        ret.x = p1.x*r/rtot;
        ret.y = p1.y*r/rtot;

        ret += p;

        return ret;
    }
};
ld sq(ld x)
{
    return x*x;
}
ld dist(pointDouble p1,pointDouble p2)
{
    return sqrt(sq(p1.x-p2.x)+sq(p1.y-p2.y));
}
struct line
{
    long double a,b,c;
    void read()
    {
        cin>>a>>b>>c;
    }
    void write()
    {
        cout<<a<<" "<<b<<" "<<c<<endl;
    }
    /// make a line from two point
    void makeline(pointDouble p, pointDouble q)
    {
        a = p.y - q.y;
        b = q.x - p.x;
        c = -a * p.x - b * p.y;
        norm();
    }
    void norm()
    {
        long double z = sqrt(a * a + b * b);
        if (abs(z) > eps)
            a /= z, b /= z, c /= z;
    }
    long double dist(pointDouble p) const
    {
        return a * p.x + b * p.y + c;
    }
};
long double det(long double a,long double b,long double c,long double d)
{
    return a*d - b*c;
}

inline bool betw(long double l,long double r,long double x)
{
    return min(l, r) <= x + eps && x <= max(l, r) + eps;
}

inline bool intersect_1d(long double a,long double b,long double c,long double d)
{
    if (a > b)
        swap(a, b);
    if (c > d)
        swap(c, d);
    return max(a, c) <= min(b, d) + eps;
}
/// for two segment
/// return false if there is no intersection else true
/// if(left==right) there is segment intersection else there is point intersection
bool intersectsegment(pointDouble a,pointDouble b,pointDouble c,
					  pointDouble d,pointDouble& left,pointDouble& right)
{
    if (!intersect_1d(a.x, b.x, c.x, d.x) || !intersect_1d(a.y, b.y, c.y, d.y))
        return false;
    line m,n;
    m.makeline(a,b);
    n.makeline(c,d);
    long double zn = det(m.a, m.b, n.a, n.b);
    if (abs(zn) < eps)
    {
        if (abs(m.dist(c)) > eps || abs(n.dist(a)) > eps)
            return false;
        if (b < a)
            swap(a, b);
        if (d < c)
            swap(c, d);
        left = max(a, c);
        right = min(b, d);
        return true;
    }
    else
    {
        left.x = right.x = -det(m.c, m.b, n.c, n.b) / zn;
        left.y = right.y = -det(m.a, m.c, n.a, n.c) / zn;
        return betw(a.x, b.x, left.x) && betw(a.y, b.y, left.y) &&
               betw(c.x, d.x, left.x) && betw(c.y, d.y, left.y);
    }
}
/// for two line
bool intersect(line m, line n, pointDouble & res)
{
    long double zn = det(m.a, m.b, n.a, n.b);
    if (abs(zn) < eps)
        return false;
    res.x = -det(m.c, m.b, n.c, n.b) / zn;
    res.y = -det(m.a, m.c, n.a, n.c) / zn;
    return true;
}

bool parallel(line m, line n)
{
    return abs(det(m.a, m.b, n.a, n.b)) < eps;
}

bool equivalent(line m, line n)
{
    return abs(det(m.a, m.b, n.a, n.b)) < eps
           && abs(det(m.a, m.c, n.a, n.c)) < eps
           && abs(det(m.b, m.c, n.b, n.c)) < eps;
}
struct circle{
    pointDouble cent;
    ld r;
    void read()
    {
        cent.read();
        cin>>r;
    }
    void write()
    {
        cout<<cent.x<<" "<<cent.y<<" "<<r<<endl;
    }
};

pointDouble get_circle_center(ld bx, ld by,ld cx, ld cy)
{
    ld B = bx * bx + by * by;
    ld C = cx * cx + cy * cy;
    ld D = bx * cy - by * cx;
    return { (cy * B - by * C) / (2.0 * D),
            (bx * C - cx * B) / (2.0 * D) };
}

circle circle_from(const pointDouble& A, const pointDouble& B,
                const pointDouble& C)
{
    pointDouble I = get_circle_center(B.x - A.x, B.y - A.y,
                                C.x - A.x, C.y - A.y);

    I.x += A.x;
    I.y += A.y;
    return { I, dist(I, A) };
}
/// return the number of intersection and the point in p1,p2 if there exist
/// (x-x1)^2 + (y-y1)^2 =r1^2
int intersectcircleline(circle cir,line m,pointDouble& p1,pointDouble& p2)
{
    /// this function do as cnter is 0,0 so we must move parameter center to 0,0
    /// a-=cent;b-=cent; a,b is two point of line m;
    /// and when output we should return it back

    long double a, b, c;
    a=m.a;
    b=m.b;
    c=m.c+(a*cir.cent.x+b*cir.cent.y);
    long double x0 = -a*c/(a*a+b*b), y0 = -b*c/(a*a+b*b);
    if (c*c > cir.r*cir.r*(a*a+b*b)+eps)
        return 0;
    else if (abs (c*c - cir.r*cir.r*(a*a+b*b)) < eps)
    {
        p1.x=x0+cir.cent.x;
        p1.y=y0+cir.cent.y;
        return 1;
    }
    else
    {
        long double d = cir.r*cir.r - c*c/(a*a+b*b);
        long double mult = sqrt (d / (a*a+b*b));
        long double ax, ay, bx, by;
        ax = x0 + b * mult;
        bx = x0 - b * mult;
        ay = y0 - a * mult;
        by = y0 + a * mult;
        p1.x=ax+cir.cent.x;
        p1.y=ay+cir.cent.y;
        p2.x=bx+cir.cent.x;
        p2.y=by+cir.cent.y;

        return 2;
    }
}
/// return -1 if there is infinity number of intersection
/// just neep two circle without minus the center or any thing
int intersectcirclecircl(circle cir1,circle cir2,pointDouble& p1,pointDouble& p2)
{
    if(cir1.cent==cir2.cent)
    {
        if(cir1.r!=cir2.r)
            return 0;
        else
            return -1;
    }
    cir2.cent-=cir1.cent;
    ld a=-2*cir2.cent.x;
    ld b=-2*cir2.cent.y;
    ld c=(cir2.cent.x*cir2.cent.x)+
		 (cir2.cent.y*cir2.cent.y)+
		 (cir1.r*cir1.r)-(cir2.r*cir2.r);
    line m;
    m.a=a;m.b=b;m.c=c;
    circle cir3;
    cir3.cent.x=0;
    cir3.cent.y=0;
    cir3.r=cir1.r;
    int f=intersectcircleline(cir3,m,p1,p2);
    p1+=cir1.cent;
    p2+=cir1.cent;
    return f;
}
circle cirle_same_angle(circle cir1,circle cir2)
{
    ld a=(-cir1.cent.x*sq(cir2.r)+cir2.cent.x*sq(cir1.r))/(sq(cir2.r)-sq(cir1.r));
    ld b=(-cir1.cent.y*sq(cir2.r)+cir2.cent.y*sq(cir1.r))/(sq(cir2.r)-sq(cir1.r));
    ld c=(sq(cir1.r)*(sq(cir2.cent.x)+sq(cir2.cent.y))-
		  sq(cir2.r)*(sq(cir1.cent.x)+sq(cir1.cent.y)))/(sq(cir2.r)-sq(cir1.r));
    ld r=c+sq(a)+sq(b);
    circle cir;
    cir.cent.x=-a;
    cir.cent.y=-b;
    cir.r=sqrt(r);
    return cir;
}
line line_same_angle(circle cir1,circle cir2)
{
    pointDouble mid=cir1.cent.moveTowards(cir2.cent,cir1.cent.dist(cir2.cent)/2.0);
    pointDouble p=cir1.cent.rotateAboutRadian(mid,pi/2.0);
    line l;
    l.makeline(mid,p);
    return l;
}
