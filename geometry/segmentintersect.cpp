/*
  _____                                 _   _____       _                          _   
 / ____|                               | | |_   _|     | |                        | |  
| (___   ___  __ _ _ __ ___   ___ _ __ | |_  | |  _ __ | |_ ___ _ __ ___  ___  ___| |_ 
 \___ \ / _ \/ _` | '_ ` _ \ / _ \ '_ \| __| | | | '_ \| __/ _ \ '__/ __|/ _ \/ __| __|
 ____) |  __/ (_| | | | | | |  __/ | | | |_ _| |_| | | | ||  __/ |  \__ \  __/ (__| |_ 
|_____/ \___|\__, |_| |_| |_|\___|_| |_|\__|_____|_| |_|\__\___|_|  |___/\___|\___|\__|
              __/ |                                                                    
             |___/                                                                     

*/

/// the code here is to find two segment which intersection 
/// or return -1,-1 if there is no two segment like this
 
struct seg {
    pointDouble p, q;
    int id;
 
    ld get_y(ld x) const {
        if (abs(p.x - q.x) < eps)
            return p.y;
        return p.y + (q.y - p.y) * (x - p.x) / (q.x - p.x);
    }
};
 
int vec(const pointDouble& a, const pointDouble& b, const pointDouble& c) {
    ld s = (b.x - a.x) * (c.y - a.y) - (b.y - a.y) * (c.x - a.x);
    return abs(s) < eps ? 0 : s > 0 ? +1 : -1;
}
 
bool intersect(const seg& a, const seg& b)
{
    return intersect_1d(a.p.x, a.q.x, b.p.x, b.q.x) &&
           intersect_1d(a.p.y, a.q.y, b.p.y, b.q.y) &&
           vec(a.p, a.q, b.p) * vec(a.p, a.q, b.q) <= 0 &&
           vec(b.p, b.q, a.p) * vec(b.p, b.q, a.q) <= 0;
}
 
bool operator<(const seg& a, const seg& b)
{
    ld x = max(min(a.p.x, a.q.x), min(b.p.x, b.q.x));
    return a.get_y(x) < b.get_y(x) - eps;
}
 
struct event {
    ld x;
    int tp, id;
 
    event() {}
    event(ld x, int tp, int id) : x(x), tp(tp), id(id) {}
 
    bool operator<(const event& e) const {
        if (abs(x - e.x) > eps)
            return x < e.x;
        return tp > e.tp;
    }
};
 
set<seg> s;
vector<set<seg>::iterator> where;
 
set<seg>::iterator prev(set<seg>::iterator it) {
    return it == s.begin() ? s.end() : --it;
}
 
set<seg>::iterator next(set<seg>::iterator it) {
    return ++it;
}
 
pair<int, int> solve(const vector<seg>& a) {
    int n = (int)a.size();
    vector<event> e;
    for (int i = 0; i < n; ++i) {
        e.push_back(event(min(a[i].p.x, a[i].q.x), +1, i));
        e.push_back(event(max(a[i].p.x, a[i].q.x), -1, i));
    }
    sort(e.begin(), e.end());
 
    s.clear();
    where.resize(a.size());
    for (size_t i = 0; i < e.size(); ++i) {
        int id = e[i].id;
        if (e[i].tp == +1) {
            set<seg>::iterator nxt = s.lower_bound(a[id]), prv = prev(nxt);
            if (nxt != s.end() && intersect(*nxt, a[id]))
                return make_pair(nxt->id, id);
            if (prv != s.end() && intersect(*prv, a[id]))
                return make_pair(prv->id, id);
            where[id] = s.insert(nxt, a[id]);
        } else {
            set<seg>::iterator nxt = next(where[id]), prv = prev(where[id]);
            if (nxt != s.end() && prv != s.end() && intersect(*nxt, *prv))
                return make_pair(prv->id, nxt->id);
            s.erase(where[id]);
        }
    }
 
    return make_pair(-1, -1);
}
