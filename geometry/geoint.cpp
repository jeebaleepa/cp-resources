/*
  _____           _____       _   
 / ____|         |_   _|     | |  
| |  __  ___  ___  | |  _ __ | |_ 
| | |_ |/ _ \/ _ \ | | | '_ \| __|
| |__| |  __/ (_) || |_| | | | |_ 
 \_____|\___|\___/_____|_| |_|\__|
                                  
*/

struct point
{
    ll x,y;
    void read()
    {
        scl(x);
        scl(y);
    }
    point operator +(const point& b)const
    {
        return point { x+b.x, y+b.y };
    }
    void operator += (const point& b )
    {
        x += b.x;
        y += b.y;
    }
    bool operator == (const point& b)
    {
        return x == b.x && y == b.y;
    }
    bool operator != (const point& b)
    {
        return !( (*this) == b );
    }

    point operator -(const point& b)const
    {
        return point{x-b.x,y-b.y};
    }
    void operator -=(const point& b)
    {
        x-=b.x;
        y-=b.y;
    }
    ll operator *(const point& b)const
    {
        return x*b.y-y*b.x;
    }
    bool operator<(const point& p) const
    {
        return x < p.x  || ( x==p.x && y < p.y );
    }
    ll triangle(const point& b,const point& c)const
    {
        /// return 2*the area of this triangle
        /// but we should take it in abs()
        return (b-*this)*(c-*this);
    }
};
ll side(point a,point b,point c)
{
    /// if c on the right of a->b return -1;
    /// if c on the left of a->b return 1;
    /// if c on the line a->b return 0;
    if(a.triangle(b,c)==0)
        return 0;
    else if(a.triangle(b,c)<0)
        return -1;
    else
        return 1;

}
ll isin(point a,point b,point c)
{
    /// return 1 if c is in the segment [a,b]
    /// return 0 on the other case
    if(side(a,b,c)!=0)
        return 0;
    if(c.x<min(a.x,b.x) ||
            c.x>max(a.x,b.x) ||
            c.y<min(a.y,b.y) ||
            c.y>max(a.y,b.y) )
        return 0;
    return 1;
}

ll sq(ll a) {
    return a * a;
}
ll distance(const point& a, const point& b) {
    return sq(a.x-b.x)+sq(a.y-b.y);
}

ll intersection(point a,point b,point c,point d)
{
    /// return 1 if segment [a,b] and segment [c,d] intersection
    /// return 0 on the other case
    if((b-a)*(d-c)==0)
    {
        if(side(a,b,c)!=0)
            return 0;
        if(max(c.x,d.x)<min(a.x,b.x) ||
                min(c.x,d.x)>max(a.x,b.x) ||
                max(c.y,d.y)<min(a.y,b.y) ||
                min(c.y,d.y)>max(a.y,b.y) )
            return 0;
        return 1;
    }
    ll x=side(a,b,c);
    ll y=side(a,b,d);
    if((x>0 && y>0) || (x<0 && y<0))
        return 0;
    x=side(c,d,a);
    y=side(c,d,b);
    if((x>0 && y>0) || (x<0 && y<0))
        return 0;
    return 1;

}

ll polygonAreaTimes2(vector <point> &v)
{
    /// calculate the area of polygon *2
    int sz = (int)(v.size());
    ll ret = 0;

    for(int i=0;i<sz-1;i++)
        ret += v[i]*v[i+1];

    ret += v[sz-1]*v[0];

    return abs(ret);
}

int pointInsidePolygon(vector <point> &v,point p)
{
    /// 0 Outside, 1 inside and 2 on boundary
    int sz = (int)(v.size());

    for(int i=0;i<sz;i++)
        if( isin(v[i],v[i+1==sz?0:i+1],p) )
            return 2;

    point p2 = {p.x+1,3000000009ll};
    int cnt = 0;
    for(int i=0;i<sz;i++)
        cnt += intersection(p,p2,v[i],v[i+1==sz?0:i+1]);

    return cnt%2;
}


ll latticePointsOnBoundaryOfPolygon(vector <point> &v)
{
    int sz = (int)(v.size());
    ll ret = 0;
    for(int i=0;i<sz;i++)
    {
        point p1 = v[i];
        point p2 = v[i+1==sz?0:i+1];

        ll diffx = abs(p1.x-p2.x);
        ll diffy = abs(p1.y-p2.y);

        ll g = __gcd(diffx,diffy);

        ret += g;
    }

    return ret;
}

ll latticePointsInsidePolygon(vector <point> &v)
{
    ll area = polygonAreaTimes2(v);
    ll boundary = latticePointsOnBoundaryOfPolygon(v);

    return (area-boundary+2ll)/2ll;
}
/// make a polygon to a convex
vector<point> makeconvex(vector<point>p)
{
    int n=p.size();
    vector<point>v;
    sort(p.begin(),p.end());
    point a,b,c;
    for(int t=0;t<2;t++)
    {
    for(int i=0;i<n;i++)
    {
        c=p[i];
        while(v.size()>=2)
        {
            a=*(v.end()-2);
            b=*(v.end()-1);
            if(side(a,c,b)==1)
            {
                v.pop_back();
            }
            else
            {
                break;
            }
        }
        v.push_back(c);
    }
    v.pop_back();
    reverse(p.begin(),p.end());
    }
    return v;
}
/// find the minimal dis pair of point
ll smallest_distance(vector<point>p,vector<point>points_by_y)
{
    /// in the main write this 
    /*
    /// sort according x
    sort(p.begin(), p.end(), [&](const point& a, const point& b) {
        return make_pair(a.x, a.y) < make_pair(b.x, b.y);
    });

    /// sort according y
    vector<point> points_by_y = p;
    sort(points_by_y.begin(), points_by_y.end(), [&](const point& a, const point& b) {
        return a.y < b.y;
    });
    ll ans=smallest_distance(p, points_by_y);
    */
    int n=p.size();
    if(n<=1)
    {
        return ll_max;
    }
    vector<point> left = vector<point>(p.begin(), p.begin() + n / 2);
    vector<point> right = vector<point>(p.begin() + n / 2, p.end());
    vector<point> left_by_y, right_by_y;
    for(point c : points_by_y) {
        if(make_pair(c.x, c.y) <= make_pair(left.back().x, left.back().y)) {
            left_by_y.push_back(c);
        }
        else {
            right_by_y.push_back(c);
        }
    }
    long long d1 = smallest_distance(left, left_by_y);
    long long d2 = smallest_distance(right, right_by_y);
    long long d = min(d1, d2);
    int middle_x = left.back().x; // can be right[0].x or anything in between
    vector<point> stripe;
    for(point c : points_by_y) {
        if(sq(c.x-middle_x) < d) {
            stripe.push_back(c);
        }
    }
    for(int i = 0; i < (int) stripe.size(); ++i) {
        // every stripe[i], pair up with points above it by at most d
        // lemma says that the following for-loop is O(1)
        for(int j = i + 1; j < (int) stripe.size() &&
			sq(stripe[j].y-stripe[i].y) < d; ++j) {
            d = min(d, distance(stripe[i], stripe[j]));
        }
    }
    return d;
}
