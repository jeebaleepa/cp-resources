/*
 _____      _       _   _                     _   _             
|  __ \    (_)     | | | |                   | | (_)            
| |__) |__  _ _ __ | |_| |     ___   ___ __ _| |_ _  ___  _ __  
|  ___/ _ \| | '_ \| __| |    / _ \ / __/ _` | __| |/ _ \| '_ \ 
| |  | (_) | | | | | |_| |___| (_) | (_| (_| | |_| | (_) | | | |
|_|   \___/|_|_| |_|\__|______\___/ \___\__,_|\__|_|\___/|_| |_|

*/

#include <bits/stdc++.h>
#define all(x) x.begin(),x.end()
#define sc(x) scanf("%d",&x)
#define scl(x) scanf("%lld",&x)
#define pr(x) printf("%d\n",x)
#define prl(x) printf("%lld\n",x)
#define yes printf("YES\n")
#define no printf ("NO\n")
#define ll long long
#define ld long double
#define pb push_back
#define f first
#define s second
#define tmp (l+r)/2
#define fast ios::sync_with_stdio(0);cin.tie(0);cout.tie(0);
const ld pi=2*acos(0);
const ll mod=1e9+7;
const ld eps=1e-9;
const ll ll_max=2e18;
///const ll mod=998244353;

using namespace std;
int const N=400400;

bool ge(const ll& a, const ll& b) { return a >= b; }
bool le(const ll& a, const ll& b) { return a <= b; }
bool eq(const ll& a, const ll& b) { return a == b; }
bool gt(const ll& a, const ll& b) { return a > b; }
bool lt(const ll& a, const ll& b) { return a < b; }
int sgn(const ll& x) { return le(x, 0) ? eq(x, 0) ? 0 : -1 : 1; }

int n,m;
struct point {
    ll x, y;
    point() {}
    point(ll _x, ll _y) : x(_x), y(_y) {}
    void read(){
        scanf("%lld%lld",&x,&y);
    }
    void write()
    {
        printf("%lld %lld\n",x,y);
    }
    point operator-(const point& a) const { return point(x - a.x, y - a.y); }
    ll dot(const point& a) const { return x * a.x + y * a.y; }
    ll dot(const point& a, const point& b) const { return (a - *this).dot(b - *this); }
    ll cross(const point& a) const { return x * a.y - y * a.x; }
    ll cross(const point& a, const point& b) const {return (a-*this).cross(b-*this);}
    bool operator==(const point& a) const { return a.x == x && a.y == y; }
    bool operator!=(const point& a) const { return !(a==(*this)); }
    bool operator < (const point p)const { return x<p.x || (x==p.x && y<p.y);}
};
struct edge{
    point l,r;
    int type;
    int idx;
    int poly_ind;
    void write()
    {
        l.write();
        r.write();

    }
    /// type==1 inner edge
    /// type==0 outer edge
    bool operator < (const edge edge2) const
    {
        edge edge1=(*this);
        const point a = edge1.l, b = edge1.r;
        const point c = edge2.l, d = edge2.r;
        if(a!=b && c!=d &&(b==c || a==d))
            return a<c || (a==c && b<d);
        ll val = sgn(a.cross(b, c)) + sgn(a.cross(b, d));
        if (val != 0)
            return val > 0;
        val = sgn(c.cross(d, a)) + sgn(c.cross(d, b));
        return val < 0;
    }

};

struct event{
    int type;
    ll pos;
    int idx;

    /// type : 4==vert   3==add   2==get   1==remove
    bool operator < (const event e) const
    {
        if(type!=e.type)
            return type>e.type;
        return idx<e.idx;
    }
};
int isin(point p1,point p2,point p3)
{
    /// check if p3 in [p1,p2]
    if(sgn(p1.cross(p2, p3))==0 )
        return 1;
    return 0;
}
vector<edge> poly[N];
edge all_edge[N];
point query[N];
struct seg{
    ll x,y;
    int idx;
    bool operator < (const seg p1) const
    {
        return x<p1.x ||(x==p1.x && y<p1.y) || (x==p1.x && y==p1.y && idx<p1.idx);
    }
};

map<ll,vector<event>>events;
set<seg>vert_edge;
set<edge> other_edge;
int ans[N];
/// count the number of point in each polygon
/// cin>> points
/// cin>> number of polygon
/// for each polygon input vertices

int main()
{
    /// counter clock wise
    int q;
    sc(q);
    for(int i=1;i<=q;i++)
        query[i].read();
    sc(n);
    int c=0;
    for(int i=1;i<=n;i++)
    {
        vector<point>v;
        sc(m);
        for(int j=0;j<m;j++)
        {
            point p;
            p.read();
            v.pb(p);
        }
        for(int j=0;j<m;j++)/// make the polygon
        {
            int k=j+1;
            if(k==m)
                k=0;
            edge e;
            e.l=v[j];
            e.r=v[k];
            e.type=0;
            e.poly_ind=i;
            c++;
            e.idx=c;
            if(e.l.x<e.r.x)
                e.type=1;
            e.l=min(v[j],v[k]);
            e.r=max(v[j],v[k]);
            all_edge[c]=e;
            poly[i].pb(e);
        }
    }
    for(int i=1;i<=n;i++)/// for each polygon
    {
        for(edge ed:poly[i])
        {
            event e;
            if(ed.l.x==ed.r.x)
            {
                e.idx=ed.idx;
                e.type=4;
                e.pos=ed.l.x;
                events[ed.l.x].pb(e);

            }
            else
            {
                e.idx=ed.idx;

                e.type=3;
                e.pos=ed.l.x;
                events[ed.l.x].pb(e);

                e.type=1;
                e.pos=ed.r.x;
                events[ed.r.x].pb(e);
            }
        }
    }
    int cnt=0;
    for(int i=1;i<=q;i++)
    {
        event e;
        e.type=2;
        e.pos=query[i].x;
        e.idx=i;
        events[e.pos].pb(e);
        cnt++;
    }
    ll last=-2e18;
    for(auto c:events)
    {
        sort(all(c.s));
        for(auto e:c.s)
        {
            if(cnt==0)
                break;
            if(e.pos!=last)
                vert_edge.clear();
            last=e.pos;
            if(e.type==4)/// vert
            {
                edge f=all_edge[e.idx];
                seg s;
                s.x=f.l.y;
                s.y=f.r.y;
                s.idx=f.poly_ind;
                vert_edge.insert(s);
            }
            if(e.type==3)/// add
            {
                other_edge.insert(all_edge[e.idx]);
            }
            if(e.type==1)/// remove
            {
                other_edge.erase(all_edge[e.idx]);
            }
            if(e.type==2)/// get
            {
                cnt--;
                edge f;
                point p=query[e.idx];
                f.l = f.r = p;
                auto it = other_edge.upper_bound(f);
                if (it != other_edge.begin())
                {
                    f=*(--it);
                    if(isin(f.l,f.r,p) || f.type==1)
                    {
                        ans[f.poly_ind]++;
                        continue;
                    }

                }
                seg k;
                k.x = k.y = p.y;
                k.idx=0;
                auto ve=vert_edge.upper_bound(k);
                if(ve != vert_edge.begin())
                {
                    k=*(--ve);
                    if(p.y>=k.x && p.y<=k.y)
                    {
                        ans[k.idx]++;
                        continue;
                    }
                }
            }
        }
    }
    for(int i=1;i<=n;i++)
    {
        pr(ans[i]);
    }
    return 0;
}
