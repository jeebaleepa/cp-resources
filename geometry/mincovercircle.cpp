/*
 __  __ _        _____                     _____ _          _      
|  \/  (_)      / ____|                   / ____(_)        | |     
| \  / |_ _ __ | |     _____   _____ _ __| |     _ _ __ ___| | ___ 
| |\/| | | '_ \| |    / _ \ \ / / _ \ '__| |    | | '__/ __| |/ _ \
| |  | | | | | | |___| (_) \ V /  __/ |  | |____| | | | (__| |  __/
|_|  |_|_|_| |_|\_____\___/ \_/ \___|_|   \_____|_|_|  \___|_|\___|

*/

vector<point>p;
vector<point>v;
void minimum_covering_circle()
{
    int n=p.size();
    if(n==0 || n==1)
    {
        printf("0.00");
        return ;
    }
    if(n==2)
    {

        printf("%.2Lf",dist(p[0],p[1]));
        return ;
    }
    point cent;
    cent.x=0.0;
    cent.y=0.0;
    ld r=0.0;
    for(point c:p)
    {
        r=max(r,dist(cent,c));
    }
    for(auto c:p)
        if(abs(dist(cent,c)-r)<eps2)
            v.pb(c);
    int w=v.size();
    if(w>=3 || (w==2 && v[0].sideofpoint(v[1],cent)==0))
    {
        printf("%.2Lf",r*2.0);
        return ;
    }
    if(w==1)
    {
        line l;
        l.makeline(cent,v[0]);
        ld mov=dist(cent,v[0]);
        for(int i=0;i<n;i++)
        {
            if(p[i]==v[0])
                continue;
            point mid=p[i]+v[0];
            mid.x/=2.0;
            mid.y/=2.0;
            point q=v[0].rotateAboutRadian(mid,pi/2.0);
            line l2;
            l2.makeline(mid,q);
            intersect(l,l2,q);
            mov=min(mov,dist(cent,q));
        }
        cent=cent.moveTowards(v[0],mov);
        r=dist(cent,v[0]);
        v.clear();
        for(auto c:p)
            if(abs(dist(cent,c)-r)<eps2)
                v.pb(c);
        w=v.size();
        if(w>=3 || (w==2 && v[0].sideofpoint(v[1],cent)==0))
        {
            ld d=dist(cent,v[0]);
            printf("%.2Lf",d*2.0);
            return ;
        }
    }
    point mid=v[0]+v[1];
    mid.x/=2.0;
    mid.y/=2.0;
    ld mov=dist(cent,mid);
    line l;
    l.makeline(cent,mid);
    for(int i=0;i<n;i++)
    {
        if(p[i]==v[0] || p[i]==v[1])
            continue;
        if(dist(p[i],mid)-dist(mid,v[0])<eps)
            continue;
        point mid2=p[i]+v[0];
        mid2.x/=2.0;
        mid2.y/=2.0;
        point q=v[0].rotateAboutRadian(mid2,pi/2.0);
        line l2;
        l2.makeline(mid2,q);
        intersect(l,l2,q);
        mov=min(mov,dist(cent,q));
    }
    cent=cent.moveTowards(mid,mov);
    ld d=dist(v[0],cent);
    printf("%.2Lf",d*2.0);
    return ;
}
