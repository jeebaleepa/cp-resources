/*
 _    _       _              _____ _          _      _                
| |  | |     (_)            / ____(_)        | |    | |               
| |  | |_ __  _  ___  _ __ | |     _ _ __ ___| | ___| |     ___ _ __  
| |  | | '_ \| |/ _ \| '_ \| |    | | '__/ __| |/ _ \ |    / _ \ '_ \ 
| |__| | | | | | (_) | | | | |____| | | | (__| |  __/ |___|  __/ | | |
 \____/|_| |_|_|\___/|_| |_|\_____|_|_|  \___|_|\___|______\___|_| |_| 

*/

int n;
ld w,h;
struct point{
    ld x,y;
    void read()
    {
        cin>>x>>y;
    }
};
struct circle{
    point cent;
    ld r;
    void read()
    {
        cent.read();
        cin>>r;
    }
};
circle c[2020];
ld sq(ld x)
{
    return x*x;
}
ld dist(point p1,point p2)
{
    return sqrt(sq(p1.x-p2.x)+sq(p1.y-p2.y));
}
struct event{
    ld teta;
    int type;
    bool operator < (const event &e) const
    {
        if(abs(teta-e.teta)<eps)
            return type>e.type;
        return teta<e.teta;
    }
};
vector<event> v;

void bad(ld st,ld en)
{
    if(st-2.0*pi>eps)
        st-=2.0*pi;
    if(en-2.0*pi>eps)
        en-=2.0*pi;
    if(st<-eps)
        st+=2.0*pi;
    if(en<-eps)
        en+=2.0*pi;


    if(st-en>eps)
    {
        bad(st,2.0*pi);
        bad(0.0,en);
        return ;
    }
    event e;
    e.type =1;
    e.teta =st;
    v.pb(e);
    e.type =-1;
    e.teta =en;
    v.pb(e);
}
void union_of_len()
{
    cin>>w>>h>>n;

    for(int i=0;i<n;i++)
        c[i].read();
    ld ans=0;
    for(int i=0;i<n;i++)
    {
        v.clear();
        ld x,y,r;
        x=c[i].cent.x;
        y=c[i].cent.y;
        r=c[i].r;
        if(x-r-w>=-eps || y-r-h>=-eps)
            continue;
        /// right side
        if(r-abs(w-x)>eps)
        {
            ld teta=acos(abs(x-w)/r);
            if(x-w>eps)
                bad(pi+teta,pi-teta);
            else
                bad(2.0*pi-teta,teta);
        }
        /// up side
        if(r-abs(h-y)>eps)
        {
            ld teta=acos(abs(y-h)/r);
            if(h-y>eps)
                bad(pi/2.0-teta,pi/2.0+teta);
            else
                bad(3.0*pi/2.0+teta,3.0*pi/2.0-teta);
        }
        /// left side
        if(r-x>eps)
        {
            ld teta=acos(x/r);
            bad(pi-teta,pi+teta);
        }
        /// down side
        if(r-y>eps)
        {
            ld teta=acos(y/r);
            bad(3.0*pi/2.0-teta,3.0*pi/2.0+teta);
        }
        /// with circle
        int f=0;
        for(int j=0;j<n;j++)
        {
            if(j==i)
                continue;
            ld x2=c[j].cent.x;
            ld y2=c[j].cent.y;
            ld r2=c[j].r;
            ld d=dist(c[i].cent,c[j].cent);
            if(r2-d-r>=-eps)
            {
                f=1;
                break;
            }
            if(r-d-r2>-eps || d-r-r2>=-eps)
                continue;
            ld init;
            if(abs(x-x2)==0)
                if(y2-y>-eps)
                    init=pi/2.0;
                else
                    init=3.0*pi/2.0;
            else if(x-x2>-eps)
                if(y-y2>-eps)
                    init=pi+acos(abs(x-x2)/d);
                else
                    init=pi-acos(abs(x-x2)/d);
            else
                if(y2-y>-eps)
                    init=acos(abs(x-x2)/d);
                else
                    init=2.0*pi-acos(abs(x-x2)/d);
            ld teta=acos((sq(r)+sq(d)-sq(r2))/(2.0*d*r));
            ///cout<<(init-teta)*180.0/pi<<" "<<(init+teta)*180.0/pi<<endl;
            bad(init-teta,init+teta);
        }
        if(f)
            continue;
        sort(all(v));
        ld good=2.0*pi;
        ld cur=0;
        int cnt=0;
        for(event c:v)
        {
            if(c.type==1)
            {
                cnt++;
                if(cnt==1)
                    cur=c.teta;
                else
                    continue;
            }
            if(c.type==-1)
            {
                cnt--;
                if(cnt==0)
                    good-=(c.teta-cur);
                else
                    continue;
            }

        }
        ///cout<<"good is "<<i<<" "<<good*180.0/pi<<"\n";
        ans+=r*good;
    }
    cout<<ans<<"\n";

}
