/*
 __  __       _         _____ _          _      
|  \/  |     | |       / ____(_)        | |     
| \  / | __ _| | _____| |     _ _ __ ___| | ___ 
| |\/| |/ _` | |/ / _ \ |    | | '__/ __| |/ _ \
| |  | | (_| |   <  __/ |____| | | | (__| |  __/
|_|  |_|\__,_|_|\_\___|\_____|_|_|  \___|_|\___|
                                                
*/

// Structure to represent a 2D circle
struct Circle {
    pointDouble C;
    ld R;
};

// Function to return the euclidean distance
// between two points
ld dist(const pointDouble& a, const pointDouble& b)
{
    return sqrt((a.x - b.x)*(a.x - b.x)
                + (a.y - b.y)*(a.y - b.y));
}

// The following two functions are used
// To find the equation of the circle when
// three points are given.

// Helper method to get a circle defined by 3 points
pointDouble get_circle_center(ld bx, ld by,
                        ld cx, ld cy)
{
    ld B = bx * bx + by * by;
    ld C = cx * cx + cy * cy;
    ld D = bx * cy - by * cx;
    return { (cy * B - by * C) / (2 * D),
            (bx * C - cx * B) / (2 * D) };
}

// Function to return a unique circle that
// intersects three points
Circle circle_from(const pointDouble& A, const pointDouble& B,
                const pointDouble& C)
{
    pointDouble I = get_circle_center(B.x - A.x, B.y - A.y,
                                C.x - A.x, C.y - A.y);

    I.x += A.x;
    I.y += A.y;
    return { I, dist(I, A) };
}
