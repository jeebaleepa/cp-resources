/*
 __  __            _____      _       _   ______         _____           _ _           
|  \/  |          |  __ \    (_)     | | |  ____|       |  __ \         | (_)          
| \  / | __ ___  _| |__) |__  _ _ __ | |_| |__ ___  _ __| |__) |__ _  __| |_ _   _ ___ 
| |\/| |/ _` \ \/ /  ___/ _ \| | '_ \| __|  __/ _ \| '__|  _  // _` |/ _` | | | | / __|
| |  | | (_| |>  <| |  | (_) | | | | | |_| | | (_) | |  | | \ \ (_| | (_| | | |_| \__ \
|_|  |_|\__,_/_/\_\_|   \___/|_|_| |_|\__|_|  \___/|_|  |_|  \_\__,_|\__,_|_|\__,_|___/
                                                                                       
*/

vector<pointDouble> p;
int get_number(ld r)
{
    int ans=0;
    for(auto c:p)
    {
        int cnt=0;
        vector<pair<ld,int>>ev;
        pointDouble cent=c;
        cent.x+=r/2.0;
        for(auto c2:p)
        {
            if(c==c2)
                continue;
            if(c2.dist(c)-2.0*r>eps)
                continue;

            ld a=c.dist(c2)/2.0;
            ld f=c.angleRadian(cent,c2);
            if(f<0)
                f+=2.0*pi;
            ld en=acos(a/r)+f;
            ld st=-acos(a/r)+f;
            if(st<0)
                st+=2.0*pi;
            pair<ld,int> e;
            e.f=st;
            if(e.f<0)
                e.f+=2.0*pi;
            e.s=-1;
            ev.pb(e);
            ld w=e.f;
            e.f=en;
            if(e.f<0)
                e.f+=2.0*pi;
            e.s=1;

            ev.pb(e);
            if(w-e.f>eps)
                cnt++;
        }
        sort(all(ev));
        for(auto e:ev)
        {
            cnt-=e.s;
            ans=max(ans,cnt);
            if(ans>=m-1)
                return ans+1;
        }
    }
    return ans+1;
}
