/*
  _____ _               _    _____           _     _      _                 
 / ____| |             | |  |_   _|         (_)   | |    | |                
| |    | |__   ___  ___| | __ | |  _ __  ___ _  __| | ___| |     ___   __ _ 
| |    | '_ \ / _ \/ __| |/ / | | | '_ \/ __| |/ _` |/ _ \ |    / _ \ / _` |
| |____| | | |  __/ (__|   < _| |_| | | \__ \ | (_| |  __/ |___| (_) | (_| |
 \_____|_| |_|\___|\___|_|\_\_____|_| |_|___/_|\__,_|\___|______\___/ \__, |
                                                                       __/ |
                                                                      |___/ 
                                                                      
*/

int check_inside_log(vector<pointDouble >v,pointDouble p)
{
 
    /// in main
    /*
    for(int i=0;i<n;i++)
    {
        if(v[i]<v[0])
            swap(v[0],v[i]);
    }
 
    pointDouble p0;
    p0=v[0];
    sort(v.begin()+1, v.end(),[&](const pointDouble& a, const pointDouble& b) {
        return p0.angleRadian(b,a)>0 ||
			   (abs(p0.angleRadian(b,a))<eps && p0.dist(a)<p0.dist(b));
    });
    */
    int n=v.size();
    pointDouble p0=v[0];
    int l=1;
    int r=n-1;
    if(	p0.sideofpoint(v[1],p)==-1 ||
	   (p0.sideofpoint(v[1],p)==0 && p0.dist(p)>p0.dist(v[1])) ||
		p0.sideofpoint(v[n-1],p)==1 ||
       (p0.sideofpoint(v[n-1],p)==0 && p0.dist(p)>p0.dist(v[n-1])))
        return 0;
    int tmp;
    while(l<r)
    {
        tmp=(l+r+1)/2;
        if(p0.sideofpoint(v[tmp],p)==0)
        {
            if(p0.dist(p)>p0.dist(v[tmp]))
                return 0;
            return 1;
        }
        else if(p0.sideofpoint(v[tmp],p)==1)
        {
            l=tmp;
        }
        else
        {
            r=tmp-1;
        }
    }
    if(v[l].sideofpoint(v[l+1],p)>=0)
        return 1;
    return 0;
}
 
