/*
 _____      _       _   _                     _   _          _______            
|  __ \    (_)     | | | |                   | | (_)        |__   __|           
| |__) |__  _ _ __ | |_| |     ___   ___ __ _| |_ _  ___  _ __ | |_ __ ___  ___ 
|  ___/ _ \| | '_ \| __| |    / _ \ / __/ _` | __| |/ _ \| '_ \| | '__/ _ \/ _ \
| |  | (_) | | | | | |_| |___| (_) | (_| (_| | |_| | (_) | | | | | | |  __/  __/
|_|   \___/|_|_| |_|\__|______\___/ \___\__,_|\__|_|\___/|_| |_|_|_|  \___|\___|

*/

#include <bits/stdc++.h>
#define all(x) x.begin(),x.end()
#define sc(x) scanf("%d",&x)
#define scl(x) scanf("%lld",&x)
#define pr(x) printf("%d\n",x)
#define prl(x) printf("%lld\n",x)
#define yes printf("YES\n")
#define no printf ("NO\n")
#define ll long long
#define ld long double
#define pb push_back
#define f first
#define s second
#define tmp (l+r)/2
#define fast ios::sync_with_stdio(0);cin.tie(0);cout.tie(0);
const ld pi=2*acos(0);
const ll mod=1e9+7;
const ld eps=1e-9;
const ll ll_max=2e18;
const ll mod2=998244353;
const ll mod3=1e9+9;

using namespace std;
int const N=4000400;

bool ge(const ll& a, const ll& b) { return a >= b; }
bool le(const ll& a, const ll& b) { return a <= b; }
bool eq(const ll& a, const ll& b) { return a == b; }
bool gt(const ll& a, const ll& b) { return a > b; }
bool lt(const ll& a, const ll& b) { return a < b; }
int sgn(const ll& x) { return le(x, 0) ? eq(x, 0) ? 0 : -1 : 1; }

int n,m;
struct point {
    ll x, y;
    point() {}
    point(ll _x, ll _y) : x(_x), y(_y) {}
    void read(){
        scanf("%lld%lld",&x,&y);
    }
    void write()
    {
        printf("%lld %lld\n",x,y);
    }
    point operator-(const point& a) const { return point(x - a.x, y - a.y); }
    ll dot(const point& a) const { return x * a.x + y * a.y; }
    ll dot(const point& a, const point& b) const { return (a - *this).dot(b - *this); }
    ll cross(const point& a) const { return x * a.y - y * a.x; }
    ll cross(const point& a,const point& b) const {return (a-*this).cross(b-*this);}
    bool operator==(const point& a) const { return a.x == x && a.y == y; }
    bool operator!=(const point& a) const { return !(a==(*this)); }
    bool operator < (const point p)const { return x<p.x || (x==p.x && y<p.y);}
};
struct edge{
    point l,r;
    int type;
    int idx;
    int poly_ind;
    void write()
    {
        l.write();
        r.write();

    }
    /// type==1 inner edge
    /// type==0 outer edge
    bool operator < (const edge edge2) const
    {
        edge edge1=(*this);
        const point a = edge1.l, b = edge1.r;
        const point c = edge2.l, d = edge2.r;
        if(a!=b && c!=d &&(b==c || a==d))
            return a<c || (a==c && b<d);
        ll val = sgn(a.cross(b, c)) + sgn(a.cross(b, d));
        if (val != 0)
            return val > 0;
        val = sgn(c.cross(d, a)) + sgn(c.cross(d, b));
        return val < 0;
    }

};

struct event{
    int type;
    ll pos;
    int idx;

    /// type : 5==for_par 4==vert   3==get    2==add  1==remove
    bool operator < (const event e) const
    {
        if(type!=e.type)
            return type>e.type;
        return idx<e.idx;
    }
};
int isin(point p1,point p2,point p3)
{
    /// check if p3 in [p1,p2]
    if(sgn(p1.cross(p2, p3))==0 )
        return 1;
    return 0;
}

struct seg{
    ll x,y;
    int idx;
    bool operator < (const seg p1) const
    {
        return x<p1.x ||(x==p1.x && y<p1.y) || (x==p1.x && y==p1.y && idx<p1.idx);
    }
};

vector<edge> poly[N];
edge all_edge[N];
point query[N];
map<ll,vector<event>>events;
set<edge> other_edge;
set<seg>vert_edge;
point min_point[N];
int ans[N];
int par[N];
ll po[N];
ll hs[N];
ll po2[N];
ll hs2[N];
int vis[N];
vector<int>vec[N];
/// count the number of point in each polygon
/// cin>> points
/// cin>> number of polygon
/// for each polygon input vertices
vector<ll>v;
void dfs(int node)
{
    ll cnt=0;
    for(auto c:vec[node])
        dfs(c);
    v.clear();
    for(auto c:vec[node])
        v.pb(hs[c]);
    hs[node]=1;
    sort(all(v));
    for(auto c:v)
    {
        cnt++;
        hs[node]=(hs[node]+ po[cnt]*c%mod)%mod;
    }

}
void dfs2(int node,ll x)
{
    hs2[node]=(x*mod3%mod+hs[node])%mod;
    for(auto c:vec[node])
        dfs2(c,hs2[node]);
}
int solve(int g)
{
    /// counter clock wise

    sc(n);
    for(int i=0;i<=n+10;i++)
        vec[i].clear(),poly[i].clear(),vis[i]=0;
    events.clear();
    other_edge.clear();

    int c=0;
    for(int i=1;i<=n;i++)
    {
        vector<point>v;
        sc(m);
        point h;
        h.x=h.y=1e18;

        for(int j=0;j<m;j++)
        {
            point p;
            p.read();
            v.pb(p);
            if(p<h)
                h=p;
        }
        reverse(all(v));
        min_point[i]=h;
        event q;
        q.pos=h.x;
        q.type=5;
        q.idx=i;
        events[q.pos].pb(q);
        for(int j=0;j<m;j++)/// make the polygon
        {
            int k=j+1;
            if(k==m)
                k=0;
            edge e;
            e.l=v[j];
            e.r=v[k];
            e.type=0;
            e.poly_ind=i;
            c++;
            e.idx=c;
            if(e.l.x<e.r.x)
                e.type=1;
            e.l=min(v[j],v[k]);
            e.r=max(v[j],v[k]);
            all_edge[c]=e;
            poly[i].pb(e);
        }
    }
    for(int i=1;i<=n;i++)/// for each polygon
    {
        for(edge ed:poly[i])
        {
            event e;
            if(ed.l.x==ed.r.x)
            {
                e.idx=ed.idx;
                e.type=4;
                e.pos=ed.l.x;
                events[ed.l.x].pb(e);

            }
            else
            {
                e.idx=ed.idx;

                e.type=3;
                e.pos=ed.l.x;
                events[ed.l.x].pb(e);

                e.type=1;
                e.pos=ed.r.x;
                events[ed.r.x].pb(e);
            }
        }
    }
    int q;
    sc(q);
    for(int i=1;i<=2*q;i++)
    {
        query[i].read();
        event e;
        e.type=2;
        e.pos=query[i].x;
        e.idx=i;
        events[e.pos].pb(e);
    }
    if(g==50)
        return 0;
    if(g==51)
        return 0;
    if(g==52)
        return 0;
    if(g==78)
        return 0;
    for(auto c:events)
    {
        sort(all(c.s));
        vert_edge.clear();
        for(auto e:c.s)
        {
            if(e.type==4)/// vert
            {
                edge f=all_edge[e.idx];
                seg s;
                s.x=f.l.y;
                s.y=f.r.y;
                s.idx=f.poly_ind;
                vert_edge.insert(s);
            }
            if(e.type==3)/// add
            {
                other_edge.insert(all_edge[e.idx]);
            }
            if(e.type==1)/// remove
            {
                other_edge.erase(all_edge[e.idx]);
            }
            if(e.type==2 || e.type==5)/// get
            {

                point p;
                if(e.type==5)
                    p=min_point[e.idx];
                else
                    p=query[e.idx];

                seg k;
                k.x = k.y = p.y;
                k.idx=0;
                auto ve=vert_edge.upper_bound(k);

                if(ve != vert_edge.begin())
                {

                    k=*(--ve);
                    if(p.y>=k.x && p.y<=k.y)
                    {
                        if(e.type==2)
                            ans[e.idx]=k.idx;
                        else
                            par[e.idx]=k.idx;
                        continue;
                    }
                }

                edge f;
                f.l = f.r = p;
                auto it = other_edge.upper_bound(f);
                if (it != other_edge.begin())
                {

                    f=*(--it);
                    if(f.type==1 || isin(f.l,f.r,p))
                    {
                        if(e.type==2)
                            ans[e.idx]=f.poly_ind;
                        else
                            par[e.idx]=f.poly_ind;
                        continue;
                    }
                    if(e.type== 2)
                        ans[e.idx]=par[f.poly_ind];
                    if(e.type== 5)
                        par[e.idx]=par[f.poly_ind];
                    continue;
                }

                if(e.type==2)
                    ans[e.idx]=0;
                if(e.type==5)
                    par[e.idx]=0;

            }
        }
        c.s.clear();
    }
    /// have node (zero)
    for(int i=1;i<=n;i++)
    {
        vec[par[i]].pb(i);
    }
    /// wrong with hashing
    dfs(0);
    dfs2(0,0);
    int ret=0;
    for(int i=1;i<=2*q;i++,i++)
    {
        int node1=ans[i];
        int node2=ans[i+1];

        if(hs2[node1]==hs2[node2])
            ret++;
    }
    return ret;
}

int main()
{
    freopen("input.txt","r",stdin);
    freopen("output.txt","w",stdout);
    po[0]=1;
    for(int i=1;i<N-10;i++)
        po[i]=po[i-1]*mod2%mod;
    po2[0]=1;
    for(int i=1;i<N-10;i++)
        po2[i]=po2[i-1]*mod3%mod;
    int q;
    sc(q);
    for(int i=1;i<=q;i++)
    {
        printf("Case #%d: %d\n",i,solve(i));
    }
    return 0;
}
/*
1
6
4
110 0 0 0 0 110 110 110
4
10 40 40 40 40 10 10 10
4
50 50 50 60 60 60 60 50
4
100 70 70 70 70 100 100 100
4
90 90 90 80 80 80 80 90
4
20 20 20 30 30 30 30 20
38
5 5 55 55
75 75 85 85
75 75 25 25
15 15 25 25
5 5 25 25
75 75 25 25
85 85 55 55
85 85 55 55
15 15 55 55
55 55 15 15
15 15 75 75
85 85 25 25
5 5 75 75
15 15 5 5
5 5 55 55
55 55 25 25
15 15 25 25
5 5 55 55
15 15 75 75
75 75 25 25
85 85 55 55
85 85 5 5
25 25 85 85
55 55 15 15
85 85 75 75
25 25 5 5
15 15 75 75
85 85 25 25
5 5 85 85
5 5 75 75
25 25 85 85
25 25 75 75
25 25 85 85
25 25 75 75
25 25 5 5
85 85 15 15
75 75 85 85
55 55 85 85

*/
