/*
 _______   _                         _       _           _ 
|__   __| (_)                       | |     | |         | |
   | |_ __ _  __ _ _ __   __ _ _   _| | __ _| |_ ___  __| |
   | | '__| |/ _` | '_ \ / _` | | | | |/ _` | __/ _ \/ _` |
   | | |  | | (_| | | | | (_| | |_| | | (_| | ||  __/ (_| |
   |_|_|  |_|\__,_|_| |_|\__, |\__,_|_|\__,_|\__\___|\__,_|
                          __/ |                            
                         |___/                             
                         
*/

struct triangle{
    point p1,p2,p3;
    void fix()
    {
        /// make them counter clock wise
        if(p2<p1)
            swap(p1,p2);
        if(p3<p1)
            swap(p1,p3);
        if(side(p1,p2,p3)<0)
            swap(p2,p3);
    }
    void write()
    {
        p1.write();
        p2.write();
        p3.write();

    }
    int isin(point a)
    {
        if(side(p1,p2,a)==1 && side(p2,p3,a)==1 && side(p3,p1,a)==1)
            return 1;
        return 0;
    }
};
vector<triangle> get(vector<point>v )
{
    /// v is counter clock wise
    int n=v.size();
    vector<triangle>ans;
    vector<point>cur;
    point a,b,c;
    for(int i=0;i<n;i++)
    {
        c=v[i];
        while(cur.size()>=2)
        {
            a=*(cur.end()-2);
            b=*(cur.end()-1);
            if(side(a,b,c)==0)
            {
                cur.pop_back();
                continue;
            }
            int f=0;
            triangle t;
            t.p1=a;t.p2=b;t.p3=c;
            t.fix();
            for(auto q:v)
            {
                if(q==a || q==b || q==c)
                    continue;
                if(t.isin(q))
                {
                    f=1;
                    break;
                }
            }
            if(f==1 || side(a,b,c)<0)
                break;
            ans.pb(t);
            cur.pop_back();
        }
        cur.pb(c);
    }
    return ans;
}
