/*
 __  __       _       _     _             
|  \/  |     | |     | |   (_)            
| \  / | __ _| |_ ___| |__  _ _ __   __ _ 
| |\/| |/ _` | __/ __| '_ \| | '_ \ / _` |
| |  | | (_| | || (__| | | | | | | | (_| |
|_|  |_|\__,_|\__\___|_| |_|_|_| |_|\__, |
                                     __/ |
                                    |___/ 
                                    
*/

// O(E*sqrt(V))
int n,m,match[N],dist[N];
vector<int> g[N];

bool bfs() {
	queue<int> q;
	for(int i = 1 ; i <= n ; i++) {
		if (!match[i]) {dist[i] = 0;q.push(i);}
		else dist[i] = oo;
	}
	dist[0] = oo;
	while(!q.empty()) {
		int u = q.front();
		q.pop();
		for(auto v:g[u]) {
			if (dist[match[v]] == oo) {
				dist[match[v]] = dist[u]+1;
				q.push(match[v]);
			}
		}
	}
	return (dist[0] != oo);
}

bool dfs(int u) {
	if (!u) return 1;
	for(auto v:g[u]) {
		if (dist[match[v]] == dist[u]+1) {
			if (dfs(match[v])) {
				match[u] = v;
				match[v] = u;
				return 1;
			}
		}
	}
	dist[u] = oo;
	return 0;
}

int hopcroft_karp() {
	int matching = 0;
	while(bfs()) {
		for(int i = 1 ; i <= n ; i++) {
			if (!match[i] && dfs(i)) matching++;
		}
	}
	return matching;
}
