/*
 __  __            ______ _               
|  \/  |          |  ____| |              
| \  / | __ ___  _| |__  | | _____      __
| |\/| |/ _` \ \/ /  __| | |/ _ \ \ /\ / /
| |  | | (_| |>  <| |    | | (_) \ V  V / 
|_|  |_|\__,_/_/\_\_|    |_|\___/ \_/\_/   

*/

vector<int> g[N];
ll cap[N][N];
int par[N];

ll bfs(int src,int tar)
{
	memset(par,-1,sizeof par);
	par[src] = -2;
	queue<pair<int,ll>> q;
	q.push({src,lloo});
	while(!q.empty()) {
		auto cur = q.front();
		q.pop();
		int u = cur.F;
		ll flow = cur.S;
		for(auto v:g[u])
		{
			if (par[v] != -1 || cap[u][v] == 0) continue;
			par[v] = u;
			if (v == tar) return min(flow,cap[u][v]);
			q.push({v,min(flow,cap[u][v])});
		}
	}
	return 0;
}

ll max_flow(int src,int tar)
{
	ll flow = 0;
	ll newflow = 0;
	while(newflow = bfs(src,tar)) {
		flow += newflow;
		int cur = tar;
		while(par[cur] != -2) {
			cap[par[cur]][cur] -= newflow;
			cap[cur][par[cur]] += newflow;
			cur = par[cur];
		}
	}
	return flow;
}
