/*
  _____  _____ _____ 
 / ____|/ ____/ ____|
| (___ | |   | |     
 \___ \| |   | |     
 ____) | |___| |____ 
|_____/ \_____\_____|

*/


vector<int> g[N],tg[N];
vector<int> topo;

void dfs1(int u) {
	vis[u] = 1;
	for(auto v:g[u]) {
		if (vis[v]) continue;
		dfs1(v);
	}
	topo.pb(u);
}

int comp[N];
void dfs2(int u,int k)
{
	vis[u] = 1;
	for(auto v:tg[u])
	{
		if (vis[v]) continue;
		dfs2(v,k);
	}
	comp[u] = k;
}

int main() {
	// input
	
	for(int u = 1 ; u <= n ; u++) if (!vis[u]) dfs1(u);
	reverse(topo.begin(),topo.end());
	int cnt = 0;
	memset(vis,0,sizeof vis);
	for(auto u:topo) dfs2(u,++cnt);
}
