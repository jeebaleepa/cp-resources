/*
 _    _ _      _____  
| |  | | |    |  __ \ 
| |__| | |    | |  | |
|  __  | |    | |  | |
| |  | | |____| |__| |
|_|  |_|______|_____/ 

*/


int sz[N],par[N][MAXL],dpth[N]; // precalculated
ST segtree;

int head[N],chain[N],baseidx[N],ptr,base[N],curchain;

void HLD(int u = 1,int w = 0,int p = -1)
{
	if (head[curchain] == 0)
	{
		head[curchain] = u;
	}
	
	chain[u] = curchain;
	baseidx[u] = ptr;
	base[ptr++] = w;
	
	int heavy = 0;
	int heavysz = 0;
	int heavyw = 0;
	for(auto pp:g[u])
	{
		int v = pp.F;
		if (v == p) continue;
		if (heavysz < sz[v])
		{
			heavy = v;
			heavysz = sz[v];
			heavyw = pp.S;
		}
	}
	
	if (heavy != 0)
	{
		HLD(heavy,heavyw,u);
	}
	
	for(auto pp:g[u])
	{
		int v = pp.F;
		if (v == p || v == heavy) continue;
		curchain++;
		HLD(v,pp.S,u);
	}
}

// build the segtree after calling HLD

int queryup(int u,int v)
{
	if (u == v) return 0;
	int uchain = chain[u];
	int vchain = chain[v];
	int ans = -oo;
	
	while(uchain != vchain)
	{
		int tmp = segtree.query(baseidx[head[uchain]],baseidx[u]);
		ans = max(ans,tmp);
		u = head[uchain];
		u = par[u][0];
		uchain = chain[u];
	}

	if (u == v) return ans;
	int tmp = segtree.query(baseidx[v]+1,baseidx[u]);
	ans = max(ans,tmp);

	return ans;
}

int query(int u,int v)
{
	int lca = LCA(u,v);
	int ans1 = queryup(u,lca);
	int ans2 = queryup(v,lca);
	return max(ans1,ans2);
}

void update(int u,int w)
{
	segtree.upd(baseidx[u],w);
}
