/*
 _______   _      
|__   __| (_)     
   | |_ __ _  ___ 
   | | '__| |/ _ \
   | | |  | |  __/
   |_|_|  |_|\___| 
   
*/

const int CHAR = 2;

struct trie {
	trie* child[CHAR];
	int cnt;
	
	trie() {
		memset(child, 0, sizeof child);
		cnt = 0;
	}
	
	void insert(int x,int i = LOG-1) {
		cnt++;
		if (i == -1) return;
		
		int cur = (x&(1<<i)) > 0;
		if (child[cur] == 0) child[cur] = new trie();
		child[cur]->insert(x,i-1);
	}
	
	void erase(int x,int i = LOG-1) {
		cnt--;
		if (i == -1) return;
		
		int cur = (x&(1<<i)) > 0;
		child[cur]->erase(x,i-1);
		if (child[cur]->cnt == 0) {delete child[cur];child[cur] = 0;}
		return;
	}
	
	int cntSmallerThan(int x,int i = LOG-1) {
		if (i == -1) return 0;
		
		int cur = (x&(1<<i)) > 0;
		int ret = 0;
		if (cur) {
			ret += child[0] ? child[0]->cnt:0;
			ret += child[1] ? child[1]->cntSmallerThan(x,i-1):0;
		}
		else if (!cur) {
			ret += child[0] ? child[0]->cntSmallerThan(x,i-1):0;
		}
		return ret;
	}
	
	int kth(int k,int i = LOG-1) {
		if (k >= cnt) return -1;
		if (i == -1) return 0;

		int cnt0 = child[0] ? child[0]->cnt:0;
		
		if (k < cnt0) return child[0]->kth(k,i-1);
		return child[1]->kth(k-cnt0,i-1) + (1<<i);
	}
};
