/*
 _____              _     _    _____         _______            
|  __ \            (_)   | |  / ____|       |__   __|           
| |__) |__ _ __ ___ _ ___| |_| (___   ___  __ _| |_ __ ___  ___ 
|  ___/ _ \ '__/ __| / __| __|\___ \ / _ \/ _` | | '__/ _ \/ _ \
| |  |  __/ |  \__ \ \__ \ |_ ____) |  __/ (_| | | | |  __/  __/
|_|   \___|_|  |___/_|___/\__|_____/ \___|\__, |_|_|  \___|\___|
                                           __/ |                
                                          |___/                 
                                          
*/

struct node {
	int l,r;
	ll sum;
	node(ll _sum = 0) : sum(_sum),l(0),r(0) {}
};

struct ST {
	#define lc seg[id].l
	#define rc seg[id].r
	#define mid (l+(r-l)/2)

	vector<node> seg;
	ST() {seg.resize(n*4*20);}
	int timer = 0;

	int build(int l = 0,int r = n-1) {
		int id = ++timer;
		if (l == r) return id;
		lc = build(l,mid);
		rc = build(mid+1,r);
		seg[id].sum = seg[lc].sum + seg[rc].sum;
		return id;
	}

	int upd(int pre,int i,int x,int l = 0,int r = n-1) {
		int id = ++timer;
		seg[id] = seg[pre];
		if (l == r) {
			seg[id].sum = x;
			return id;
		}
		
		if (i <= mid) lc = upd(lc,i,x,l,mid);
		else rc = upd(rc,i,x,mid+1,r);
		seg[id].sum = seg[lc].sum + seg[rc].sum;
		return id;
	}

	ll query(int id,int L,int R,int l = 0,int r = n-1) {
		if (l > R || r < L) return 0;
		if (l >= L && r <= R) return seg[id].sum;
		return query(lc,L,R,l,mid) + query(rc,L,R,mid+1,r);
	}
};

