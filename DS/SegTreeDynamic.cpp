/*
  _____         _______            _____                              _      
 / ____|       |__   __|          |  __ \                            (_)     
| (___   ___  __ _| |_ __ ___  ___| |  | |_   _ _ __   __ _ _ __ ___  _  ___ 
 \___ \ / _ \/ _` | | '__/ _ \/ _ \ |  | | | | | '_ \ / _` | '_ ` _ \| |/ __|
 ____) |  __/ (_| | | | |  __/  __/ |__| | |_| | | | | (_| | | | | | | | (__ 
|_____/ \___|\__, |_|_|  \___|\___|_____/ \__, |_| |_|\__,_|_| |_| |_|_|\___|
              __/ |                        __/ |                             
             |___/                        |___/                               
             
*/

struct node {
	node *l,*r;
	ll sum;
	int lazy;
	
	node() : l(0), r(0), sum(0), lazy(0) {}
	
	node* left() {return l?l:(l = new node());}
	node* right() {return r?r:(r = new node());}
	
	~node() {
		delete l;
		delete r;
	}
};
 
#define mid (l+(r-l)/2)
 
void push(node *rt,int l,int r) {
	if (rt->lazy == 0) return;
	if (l != r) {
		rt->left()->lazy += rt->lazy;
		rt->right()->lazy += rt->lazy;
	}
	rt->sum += 1ll*(r-l+1)*(rt->lazy);
	rt->lazy = 0;
}
 
void pull(node *rt) {
	rt->sum = 0;
	if (rt->l) rt->sum += rt->l->sum;
	if (rt->r) rt->sum += rt->r->sum;
}
 
void update(node *rt,int L,int R,int l = 0,int r = N) {
	push(rt,l,r);
	if (l > R || r < L) return;
	else if (l >= L && r <= R) {
		rt->lazy++;
		push(rt,l,r);
		return;
	}
	update(rt->left(),L,R,l,mid);
	update(rt->right(),L,R,mid+1,r);
	
	pull(rt);
}
 
ll query(node *rt, int L,int R,int l = 0,int r = N) {
	if (rt == 0) return 0;
	push(rt,l,r);
	if (l > R || r < L) return 0;
	if (l >= L && r <= R) return rt->sum;
	
	return query(rt->l,L,R,l,mid) + query(rt->r,L,R,mid+1,r);
}
