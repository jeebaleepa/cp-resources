/*
 _____   _____ _    _ 
|  __ \ / ____| |  | |
| |  | | (___ | |  | |
| |  | |\___ \| |  | |
| |__| |____) | |__| |
|_____/|_____/ \____/ 

*/

struct DSU {
	vector<int> par, rnk, sz;
	int c;
	DSU(int n) : c(n) {
		rnk.resize(n+1);
		par.resize(n+1);
		sz.resize(n+1);
		for (int i = 1; i <= n; ++i) par[i] = i;
	}
	int find(int i) {
		return (par[i] == i ? i : (par[i] = find(par[i])));
	}
	bool same(int i, int j) {
		return find(i) == find(j);
	}
	int get_size(int i) {
		return sz[find(i)];
	}
	int count() {
		return c;    //connected components
	}
	int merge(int i, int j) {
		if ((i = find(i)) == (j = find(j))) return -1;
		else --c;
		if (rnk[i] > rnk[j]) swap(i, j);
		par[i] = j;
		sz[j] += sz[i];
		if (rnk[i] == rnk[j]) rnk[j]++;
		return j;
	}
};
