/*
 ___  _____   _____                        _______    _     _      
|__ \|  __ \ / ____|                      |__   __|  | |   | |     
   ) | |  | | (___  _ __   __ _ _ __ ___  ___| | __ _| |__ | | ___ 
  / /| |  | |\___ \| '_ \ / _` | '__/ __|/ _ \ |/ _` | '_ \| |/ _ \
 / /_| |__| |____) | |_) | (_| | |  \__ \  __/ | (_| | |_) | |  __/
|____|_____/|_____/| .__/ \__,_|_|  |___/\___|_|\__,_|_.__/|_|\___|
                   | |                                             
                   |_|                                             

*/

struct SparseTable2D {
	int st[N][N][LOG][LOG];
	int lg2[N];

	int query(int x1, int y1, int x2, int y2) {
	  x2++;
	  y2++;
	  int a = lg2[x2 - x1], b = lg2[y2 - y1];
	  return max(
			 max(st[x1][y1][a][b], st[x2 - (1 << a)][y1][a][b]),
			 max(st[x1][y2 - (1 << b)][a][b], st[x2 - (1 << a)][y2 - (1 << b)][a][b])
		   );
	}

	void build(int n, int m) { // 0 indexed
	  for (int i = 2; i < N; i++) lg2[i] = lg2[i >> 1] + 1;
	  for (int i = 0; i < n; i++) {
		for (int j = 0; j < m; j++) {
		  st[i][j][0][0] = a[i][j];
		}
	  }
	  for (int a = 0; a < LOG; a++) {
		for (int b = 0; b < LOG; b++) {
		  if (a + b == 0) continue;
		  for (int i = 0; i + (1 << a) <= n; i++) {
			for (int j = 0; j + (1 << b) <= m; j++) {
			  if (!a)
				st[i][j][a][b] = max(st[i][j][a][b - 1],
				st[i][j + (1 << (b - 1))][a][b - 1]);
			  else
				st[i][j][a][b] = max(st[i][j][a - 1][b],
				st[i + (1 << (a - 1))][j][a - 1][b]);
			}
		  }
		}
	  }
	}
};
