/*
 __  __  ____  
|  \/  |/ __ \ 
| \  / | |  | |
| |\/| | |  | |
| |  | | |__| |
|_|  |_|\____/ 

*/ 

const int BLOCK = 400;

struct Query {
	int l,r,idx;
	bool operator < (Query a) const {
		if (l/BLOCK == a.l/BLOCK)
			return ((l/BLOCK)&1) ? (r > a.r) : (r < a.r);
		return l/BLOCK < a.l/BLOCK
	}
};
