/*
 _      _  _____ _              _______            
| |    (_)/ ____| |            |__   __|           
| |     _| |    | |__   __ _  ___ | |_ __ ___  ___ 
| |    | | |    | '_ \ / _` |/ _ \| | '__/ _ \/ _ \
| |____| | |____| | | | (_| | (_) | | | |  __/  __/
|______|_|\_____|_| |_|\__,_|\___/|_|_|  \___|\___|

*/

/// make the equation y=m*x+p+g
/// where g is constant and y==dp[i]
/// dp[i]=query(x)+g;
/// Line l={m,p};
/// insert(node*,l);
/// if you want max
/// dp[i]=-query(x)+g;
/// Line l={-m,-p};
/// insert(node* ,l);
 
using namespace std;
const ll RANG = 1e5;
struct Line{
    ll m,p;
    Line(){
        m=0;
        p=ll_max;
    }
}fai;
struct node{
    int cnt;
    Line line;
    node *l;
    node *r;
    node(Line line2=fai,node *l2=NULL,node *r2=NULL,int cnt2=0)
    {
        line=line2;
        l=l2;
        r=r2;
        cnt=cnt2;
    }
};
ll f(Line l,ll x){
    return l.m * x + l.p;
}
void insert(node *cur,Line line,ll st=-RANG,ll en=RANG)
{
    ll m=st+(en-st)/2ll;
    bool left = f(line, st) < f(cur->line, st);
    bool mid = f(line, m) < f(cur->line, m);
 
    if(mid) swap(cur->line, line);
    if(cur->l==NULL)
        cur->l=new node();
    if(cur->r==NULL)
        cur->r=new node();
 
    if(en - st == 0) return;
    else if(left != mid) insert(cur->l,line, st, m);
    else insert(cur->r,line, m+1, en);
}
ll query(node *cur,ll x,ll st=-RANG,ll en=RANG)
{
    ll m = st+(en-st)/2ll;
    ll curr = f(cur->line, x);
    if(en-st==0) return curr;
    if(cur->l==NULL)
        cur->l=new node();
    if(cur->r==NULL)
        cur->r=new node();
 
    if(x<m) return min(curr, query(cur->l,x, st, m));
    else return min(curr, query(cur->r,x, m+1, en));
}
 
void merg(node* dest, node* o) {
  if(o->line.m != 0 || o->line.p != ll_max) insert(dest,o->line);
  if(o->l) merg(dest, o->l);
  if(o->r) merg(dest, o->r);
}
