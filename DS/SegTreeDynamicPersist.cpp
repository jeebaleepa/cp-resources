/*
 _____              _____              _     _    _____         _______            
|  __ \            |  __ \            (_)   | |  / ____|       |__   __|           
| |  | |_   _ _ __ | |__) |__ _ __ ___ _ ___| |_| (___   ___  __ _| |_ __ ___  ___ 
| |  | | | | | '_ \|  ___/ _ \ '__/ __| / __| __|\___ \ / _ \/ _` | | '__/ _ \/ _ \
| |__| | |_| | | | | |  |  __/ |  \__ \ \__ \ |_ ____) |  __/ (_| | | | |  __/  __/
|_____/ \__, |_| |_|_|   \___|_|  |___/_|___/\__|_____/ \___|\__, |_|_|  \___|\___|
         __/ |                                                __/ |                
        |___/                                                |___/                 
    
*/

struct node;
extern node *empt;

struct node {
	node *l,*r;
	ll sum;
	
	node() : l(this), r(this), sum(0) {}
	
	node(ll _sum,node* _l = empt,node *_r = empt) : l(_l), r(_r), sum(_sum) {}
};

node* empt = new node();

#define mid (l+(r-l)/2)

node* update(node *rt, int i,int x,int l = 0,int r = N) {
	if (l == r) return new node(x);
	node* newl = rt->l;
	node* newr = rt->r;
	if (i <= mid) newl = update(rt->l,i,x,l,mid);
	else newr = update(rt->r,i,x,mid+1,r);
	
	return new node(newl->sum + newr->sum , newl, newr);
}

ll query(node *rt, int L,int R,int l = 0,int r = N) {
	if (l > R || r < L) return 0;
	if (l >= L && r <= R) return rt->sum;
	
	return query(rt->l,L,R,l,mid) + query(rt->r,L,R,mid+1,r);
}
