/*
  _____         _______            ____             _       
 / ____|       |__   __|          |  _ \           | |      
| (___   ___  __ _| |_ __ ___  ___| |_) | ___  __ _| |_ ___ 
 \___ \ / _ \/ _` | | '__/ _ \/ _ \  _ < / _ \/ _` | __/ __|
 ____) |  __/ (_| | | | |  __/  __/ |_) |  __/ (_| | |_\__ \
|_____/ \___|\__, |_|_|  \___|\___|____/ \___|\__,_|\__|___/
              __/ |                                         
             |___/                                           
             
*/

struct node {
	ll sum;
	ll mn,mn2,mncnt;
	ll mx,mx2,mxcnt;
	ll lazy;
	node(ll x = 0) :
		sum(x),
		mn(x),mn2(lloo),mncnt(1),
		mx(x),mx2(-lloo),mxcnt(1),
		lazy(0) {}
};

struct ST {
	#define lc (id<<1)
	#define rc ((id<<1)|1)
	#define mid (l+(r-l)/2)

	vector<node> seg;
	ST() {seg.resize(n*4);}

	inline node merge(node left,node right) {
		node ret;
		ret.sum = left.sum + right.sum;
		ret.mn = left.mn < right.mn ? left.mn:right.mn;
		ret.mncnt = 0;
		ret.mncnt += (ret.mn == left.mn) ? left.mncnt:0;
		ret.mncnt += (ret.mn == right.mn) ? right.mncnt:0;
		
		ret.mx = left.mx > right.mx ? left.mx:right.mx;
		ret.mxcnt = 0;
		ret.mxcnt += (ret.mx == left.mx) ? left.mxcnt:0;
		ret.mxcnt += (ret.mx == right.mx) ? right.mxcnt:0;
		
		vector<ll> tmp;
		tmp.pb(left.mn);tmp.pb(left.mn2);
		tmp.pb(right.mn);tmp.pb(right.mn2);
		
		sort(all(tmp));
		for(int i = 0 ; i < sz(tmp) ; i++) {
			if (tmp[i] != ret.mn) {
				ret.mn2 = tmp[i];
				break;
			}
		}
		
		vector<ll> tmp2;
		tmp2.pb(left.mx);tmp2.pb(left.mx2);
		tmp2.pb(right.mx);tmp2.pb(right.mx2);
		
		sort(all(tmp2),greater<ll>());
		for(int i = 0 ; i < sz(tmp2) ; i++) {
			if (tmp2[i] != ret.mx) {
				ret.mx2 = tmp2[i];
				break;
			}
		}
		
		return ret;
	}

	inline void pull(int id) {
		seg[id] = merge(seg[lc],seg[rc]);
	}

	void build(int l = 0,int r = n-1,int id = 1) {
		if (l == r) {
			seg[id] = node(a[l]);
			return;
		}
		build(l,mid,lc);
		build(mid+1,r,rc);
		pull(id);
	}

	void pushmn(int id,ll x) {
		if (seg[id].mx <= x) return;
		seg[id].sum += (x-seg[id].mx)*seg[id].mxcnt;
		if (seg[id].mn == seg[id].mx) {
			seg[id].mn = x;
		} else if (seg[id].mn2 == seg[id].mx) {
			seg[id].mn2 = x;
		}
		seg[id].mx = x;
	}

	void pushmx(int id,ll x) {
		if (seg[id].mn >= x) return;
		seg[id].sum += (x-seg[id].mn)*seg[id].mncnt;
		if (seg[id].mx == seg[id].mn) {
			seg[id].mx = x;
		} else if (seg[id].mx2 == seg[id].mn) {
			seg[id].mx2 = x;
		}
		seg[id].mn = x;
	}

	void pushadd(int l,int r,int id) {
		if (l != r) {
			seg[lc].lazy += seg[id].lazy;
			seg[rc].lazy += seg[id].lazy;
		}
		
		seg[id].sum += seg[id].lazy*((ll)(r-l+1));
		seg[id].mn += seg[id].lazy;
		seg[id].mx += seg[id].lazy;
		if (seg[id].mn2 != lloo) seg[id].mn2 += seg[id].lazy;
		if (seg[id].mx2 != -lloo) seg[id].mx2 += seg[id].lazy;
		seg[id].lazy = 0;
	}

	void push(int l,int r,int id) {
		
		pushadd(l,r,id);
		
		if (l == r) return;
		pushadd(l,mid,lc);
		pushadd(mid+1,r,rc);
		pushmn(lc,seg[id].mx);
		pushmn(rc,seg[id].mx);
		pushmx(lc,seg[id].mn);
		pushmx(rc,seg[id].mn);
	}

	void updmx(int L,int R,ll x,int l = 0,int r = n-1,int id = 1) {
		push(l,r,id);
		if (l > R || r < L || seg[id].mn >= x) return;
		
		if (l >= L && r <= R && seg[id].mn2 > x) {
			pushmx(id,x);
			return;
		}

		updmx(L,R,x,l,mid,lc);
		updmx(L,R,x,mid+1,r,rc);
		pull(id);
	}

	void updmn(int L,int R,ll x,int l = 0,int r = n-1,int id = 1) {
		push(l,r,id);
		if (l > R || r < L || seg[id].mx <= x) return;
		
		if (l >= L && r <= R && seg[id].mx2 < x) {
			pushmn(id,x);
			return;
		}

		updmn(L,R,x,l,mid,lc);
		updmn(L,R,x,mid+1,r,rc);
		pull(id);
	}

	void updadd(int L,int R,ll x,int l = 0,int r = n-1,int id = 1) {
		push(l,r,id);
		if (l > R || r < L) return;
		
		if (l >= L && r <= R) {
			seg[id].lazy += x;
			push(l,r,id);
			return;
		}

		updadd(L,R,x,l,mid,lc);
		updadd(L,R,x,mid+1,r,rc);
		pull(id);
	}

	node query(int L,int R,int l = 0,int r = n-1,int id = 1) {
		push(l,r,id);
		if (l > R || r < L) return node();
		if (l >= L && r <= R) return seg[id];
		return merge(query(L,R,l,mid,lc),query(L,R,mid+1,r,rc));
	}
};
