/*
          _            _____                    _      _    
    /\   | |          / ____|                  (_)    | |   
   /  \  | |__   ___ | |     ___  _ __ __ _ ___ _  ___| | __
  / /\ \ | '_ \ / _ \| |    / _ \| '__/ _` / __| |/ __| |/ /
 / ____ \| | | | (_) | |___| (_) | | | (_| \__ \ | (__|   < 
/_/    \_\_| |_|\___/ \_____\___/|_|  \__,_|___/_|\___|_|\_\
                                                            
*/

string strings[N];

struct AhoCorasick {
	struct node {
		node* link;
		node* child[CHAR];
		vector<node*> tree;
		int term;
		
		node() {
			link = 0;
			memset(child, 0, sizeof child);
			term = -1;
		}
		
		node* trans(int c) {
			if (child[c] == 0)
				return link?(link->trans(c)):this;
			return child[c];
		}
		
		void insert(int id,string &str,int i = 0) {
			if (i == sz(str)) {
				term = id;
				return;
			}
			int c = str[i]-'a';
			if (child[c] == 0) child[c] = new node();
			child[c]->insert(id,str,i+1);
		}
	};
	
	node* root;

	void build(string text) {
		root = new node();
		for(int i = 0 ; i < n ; i++) root->insert(i,strings[i]);
		queue<node*> q;
		q.push(root);
		
		while(!q.empty()) {
			node* u = q.front();
			q.pop();
			for(int c = 0 ; c < 26 ; c++) {
				node* v = u->child[c];
				if (v) {
					if (u->link) v->link = u->link->trans(c);
					else v->link = u;
					v->link->tree.pb(v);
					q.push(v);
				}
			}
		}
		
		node* state = root;
		for(int i = 0 ; i < sz(text) ; i++) {
			state = state->trans(text[i]-'a');
			// do stuff
		}
	}
};
