/*
  _____        __  __ _                                     
 / ____|      / _|/ _(_)         /\                         
| (___  _   _| |_| |_ ___  __   /  \   _ __ _ __ __ _ _   _ 
 \___ \| | | |  _|  _| \ \/ /  / /\ \ | '__| '__/ _` | | | |
 ____) | |_| | | | | | |>  <  / ____ \| |  | | | (_| | |_| |
|_____/ \__,_|_| |_| |_/_/\_\/_/    \_\_|  |_|  \__,_|\__, |
                                                       __/ |
                                                      |___/  
                                                      
*/

int Log[N];
int main()
{
	Log[1] = 0;
	for(int i = 2 ; i < N ; i++)
	{
		Log[i] = Log[i/2]+1;
	}
}

struct suffix_array{
	vector<int> p,c;
	vector<int> lcp;
	string str;
	int n;
	vector<int> sp[L];
	suffix_array(string str) : str(str)
	{
		str += "$";
		n = sz(str);
		p.resize(n);
		c.resize(n);
		
		build();
		buildlcp();
		
		buildsp();
	}
	
	void build()
	{
		{
			vector<pii> vec;
			for(int i = 0 ; i < n ; i++) vec.pb({str[i],i});
			sort(all(vec));
			for(int i = 0 ; i < n ; i++) p[i] = vec[i].S;
			c[p[0]] = 0;
			for(int i = 1 ; i < n ; i++)
			{
				c[p[i]] = c[p[i-1]];
				if (vec[i].F != vec[i-1].F) c[p[i]]++;
			}
		}
		
		for(int k = 0 ; (1<<k) < n ; k++)
		{
			vector<int> new_p(n),new_c(n);
			for(int i = 0 ; i < n ; i++) p[i] = (p[i]-(1<<k)+n)%n;
			vector<int> cnt(n),pos(n);
			for(auto x:p) cnt[c[x]]++;
			pos[0] = 0;
			for(int i = 1 ; i < n ; i++) pos[i] = pos[i-1]+cnt[i-1];
			for(auto x:p)
			{
				new_p[pos[c[x]]] = x;
				pos[c[x]]++;
			}
			p = new_p;
			new_c[p[0]] = 0;
			for(int i = 1 ; i < n ; i++)
			{
				pii prev = {c[p[i-1]],c[(p[i-1]+(1<<k))%n]};
				pii now = {c[p[i]],c[(p[i]+(1<<k))%n]};
				new_c[p[i]] = new_c[p[i-1]];
				if (prev != now) new_c[p[i]]++;
			}
			c = new_c;
		}
	}
	
	void buildlcp()
	{
		lcp.resize(n);
		int k = 0;
		for(int i = 0 ; i < n-1 ; i++)
		{
			int pi = c[i];
			if (pi == 0) continue;
			int j = p[pi-1];
			while(str[i+k] == str[j+k]) k++;
			//cerr << pi << endl;
			lcp[pi] = k;
			k = max(k-1,0);
		}
	}
	
	void buildsp()
	{
		for(int i = 0 ; i < L ; i++) sp[i].resize(n);
		for(int i = 0 ; i < n ; i++) sp[0][i] = lcp[i];
		for(int j = 1 ; j < L ; j++)
		{
			for(int i = 0 ; i < n ; i++)
			{
				if (i+(1<<(j-1)) < n) sp[j][i] = min(sp[j-1][i],sp[j-1][i+(1<<(j-1))]);
			}
		}
		
	}
	
	int getlcp(int i,int j)
	{
		if (i == j)
		{
			return n-i-1;
		}
		i = c[i];
		j = c[j];
		
		if (i > j) swap(i,j);
		i++;
		int k = Log[j-i+1];
		return min(sp[k][i],sp[k][j-(1<<k)+1]);
	}
	
};
