#!/bin/bash
find . -name "*.cpp" | xargs enscript --color=1 -Ecpp -B -A 1 -C -fCourier10 --margin=5:5:5:5 --mark-wrapped-lines=arrow -T 2 --word-wrap -o - | ps2pdf - print.pdf
